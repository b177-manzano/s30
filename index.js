const express = require("express"); 
const mongoose = require("mongoose");

const app = express();
const port = 3001;

//MongoDB connection
mongoose.connect("mongodb+srv://admin:WvTYxWOqixgTDFAK@cluster0.d3kcn.mongodb.net/b177-to-do?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

//Set notification for connection success or failure
//Connection to the database
let db = mongoose.connection;

// If a connection error occured, output a message in the console
db.on("error", console.error.bind(console, "connection error"));

//If the connection is successful, output a message in the console
db.once("open", () => console.log("We're connected to the cloud databasecd"));

// Create a Task schema

const taskSchema = new mongoose.Schema({
	name : String,
	status: {
		type: String,
		//Default values are the  predefined values for a field
		default: "pending"
	}
})

//Create Models 
//Server > Schema > Database > Collection (MongoDB)
const Task = mongoose.model("Task", taskSchema);
app.use(express.json());

app.use(express.urlencoded({extended:true}));

//Create a POST route to create a new task
app.post ("/tasks", (req,res) => {
	Task.findOne({name: req.body.name} , (err,result) => {
		if (result != null && result.name == req.body.name)
			// If a document was found and the doucment's name matches the information sent via the client/postman
		//Returns a message to the client/postman
		return res.send("Duplicate task found")
		//If no document found
		else {
			//create a new taks and save it to the database
			let newTask = new Task ({
				name : req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				// If there are errors in saving
				if(saveErr){
					return console.error(saveErr);
				}
				// if no error found while creating the doucment
				else {
					return res.status(201).send("New Task Created");
				}
			})
		}
	})
})





//Create a GET Request to retrieve all the tasks
app.get("/tasks",(req,res)=>{
	Task.find ({}, (err, result) => {
		// If an error occured
		if(err){
			//Will print any errors found in the console
			return console.log (err);
		}
		// if no errors found
		else{
			return res.status(200).json({
				data:result
			})
		}
	})
})

// == activity

const userSchema = new mongoose.Schema({
	username : String,
	password : String,
	status: {
		type: String,
		//Default values are the  predefined values for a field
		default: "pending"
	}
})


const Username = mongoose.model("Username", userSchema);
app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.post ("/signup", (req,res) => {
	Username.findOne({username: req.body.username}   , (err,result) => {
		if (result != null && result.username == req.body.username && result.password == req.body.password)
			// If a document was found and the doucment's name matches the information sent via the client/postman
		//Returns a message to the client/postman
		return res.send(`User ${req.body.username} taken`)
		//If no document found
		else {
			//create a new taks and save it to the database
			let newUsername = new Username ({
				username : req.body.username ,
				password : req.body.password
			})

			newUsername.save((saveErr, savedUser) => {
				// If there are errors in saving
				if(saveErr){
					return console.error(saveErr);
				}
				// if no error found while creating the doucment
				else {
					return res.status(201).send(`User ${req.body.username} successfully registered!`);
				}
			})
		}
	})
})

app.get("/user",(req,res)=>{
	Username.find ({}, (err, result) => {
		// If an error occured
		if(err){
			//Will print any errors found in the console
			return console.log (err);
		}
		// if no errors found
		else{
			return res.status(200).json({
				data:result
			})
		}
	})
})
















//==

app.listen(port,() => console.log (`Server running at port ${port}`));



